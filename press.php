<?php require 'header.php'?>
<title>Roth Lab - Press</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(1).addClass("active");</script>

<h1>Roth Lab in the news:</h1>

<p><i class="material-icons blue-text">verified_user</i>Agency
	<i class="material-icons green-text">event_note</i>Journal
	<i class="material-icons orange-text">devices_other</i>Media
<i class="material-icons red-text">video_library</i>Video</p>


<h3>Recent:</h3>
<ul class="collapsible popout" data-collapsible="accordion">
    <?php
		function date_cmp($a, $b){$va = (string) $a->date;$vb = (string) $b->date;
			if (strtotime($va)===strtotime($vb)) {return 0;}
			return (strtotime($va)<strtotime($vb)) ? 1 : -1;
		}
		$xml = simplexml_load_file("press.xml");
		$divSXE = $xml->children();
		$divArray = array();
		foreach($divSXE->article as $d) {$divArray[] = $d;}
		usort($divArray, 'date_cmp');
    	foreach ($divArray as $article) {
			$string32 = '<li><div class="collapsible-header">';
			$string32 .= '<i class="material-icons ';
			if($article->type == "info") $string32 .= 'blue-text">verified_user';
			else if($article->type == "success") $string32 .= 'green-text">event_note';
			else if($article->type == "warning") $string32 .= 'orange-text">devices_other';
			else if($article->type == "danger") $string32 .= 'red-text">video_library';
			$string32 .= '</i>';
			$string32 .= $article->name;
			
			$string32 .= '</div><div class="collapsible-body">';
			$string32 .= '<div class="row">';
			if($article->image != ""){
				$string32 .= '<div class="col s2"><a target="_blank" href="'.$article->url.'"><img class="col s12" src="'.$article->image.'"></a></div>';
			}
			$string32 .= '<div class="col s10">';
			if(!empty($article->url)){
				$string32 .= '<blockquote>';
				$string32 .= '<a target="_blank" href="'.$article->url.'">'.$article->name.'</a>';
				if($article->press != ""){
					$string32 .= '<footer>'.$article->press.' ';
					if($article->date != "") $string32 .= '<cite title="date">'.$article->date.'</cite>';
					$string32 .= '</footer>';
				}
				$string32 .= '</blockquote>';
			}
			
			foreach ($article->subarticle as $subarticle){
				if($article->type != "danger"){
					$string32 .= '<blockquote>';
					$string32 .= '<a target="_blank" href="'.$subarticle->url.'">'.$subarticle->name.'</a>';
					$string32 .= '<footer>'.$subarticle->press.' <cite title="date">'.$subarticle->date.'</cite></footer>';
					$string32 .= '</blockquote>';
					}else{
					$string32 .= '<p>'.$subarticle->press.'</p>';
					if($subarticle->url != ""){
						$string32 .= '<p><a target="_blank" href="'.$subarticle->url.'">'.$article->name.'</a></p>';
					}
				}
			}		
			
			$string32 .= '</div>';
			$string32 .= '</div>';
			$string32 .= '</div></li>';
			echo $string32;
			
			
			/*$string32 = '<div class="panel panel-default"><div class="panel-heading" role="tab" id="headingAuto'.$counter.'"><h4 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseAuto'.$counter.'" aria-expanded="false" aria-controls="collapseAuto'.$counter.'">';
				$string32 .= $article->name;
				$string32 .= '</a> <span class="label label-default">'.$article->date.'</span> <span class="label label-'.$article->type.'">'.$article->press.'</span></h4></div>';
				$string32 .= '<div id="collapseAuto'.$counter.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAuto'.$counter.'"><div class="panel-body row">';
				if($article->image != ""){
				$string32 .= '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><a target="_blank" href="'.$article->url.'"><img class="col-lg-12 col-md-12 col-sm-12 col-xs-12" src="'.$article->image.'"></a></div>';
				}
				$string32 .= '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">';
				if(!empty($article->url)){
				$string32 .= '<blockquote>';
				$string32 .= '<p><a target="_blank" href="'.$article->url.'">'.$article->name.'</a></p>';
				if($article->press != ""){
				$string32 .= '<footer>'.$article->press.' ';
				if($article->date != "") $string32 .= '<cite title="date">'.$article->date.'</cite>';
				$string32 .= '</footer>';
				}
				$string32 .= '</blockquote>';
				}
				foreach ($article->subarticle as $subarticle){
				if($article->type != "danger"){
				$string32 .= '<blockquote>';
				$string32 .= '<p><a target="_blank" href="'.$subarticle->url.'">'.$subarticle->name.'</a></p>';
				$string32 .= '<footer>'.$subarticle->press.' <cite title="date">'.$subarticle->date.'</cite></footer>';
				$string32 .= '</blockquote>';
				}else{
				$string32 .= '<p>'.$subarticle->press.'</p>';
				if($subarticle->url != ""){
				$string32 .= '<p><a target="_blank" href="'.$subarticle->url.'">'.$article->name.'</a></p>';
				}
				}
				}
				$string32 .= '</div></div></div></div>';
			echo $string32;$counter++;*/
		}
	?>
	
</ul>
<?php require 'footer.php'?>
