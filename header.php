<!doctype HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="Official RothLab website">
		<meta name="keywords" content="roth,lab,research,pdsp">
		<meta name="author" content="rothlab">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Rothlab</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<!-- Google tag (gtag.js) -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZK6PTRY86F"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-ZK6PTRY86F');
		</script>
	</head>
	<body>
		<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
		<nav class="light-blue darken-2">
			<div class="nav-wrapper">
				<a href="index.php" class="brand-logo waves-effect waves-light">&nbsp;Roth Lab </a>
				<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
				<ul class="right hide-on-med-and-down">
					<li><a class="waves-effect waves-light" href="/rothlab/research.php">Research</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/press.php">Press</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/members.php">Members</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/publications.php">Publications</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/positions.php">Positions</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/DREADD.php">DREADDs</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/databases.php">Databases</a></li>
					<li><a class="waves-effect waves-light" href="/pdspweb">PDSP</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/UNClinks.php">UNC</a></li>
					<li><a class="waves-effect waves-light" href="/prestotango">PRESTO-Tango</a></li>
				</ul>
				<ul class="side-nav" id="mobile-demo">
					<li><a class="waves-effect waves-light" href="/rothlab/research.php">Research</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/press.php">Press</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/members.php">Members</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/publications.php">Publications</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/positions.php">Positions</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/DREADD.php">DREADDs</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/databases.php">Databases</a></li>
					<li><a class="waves-effect waves-light" href="/pdspweb">PDSP</a></li>
					<li><a class="waves-effect waves-light" href="/rothlab/UNClinks.php">UNC</a></li>
					<li><a class="waves-effect waves-light" href="/prestotango">PRESTO-Tango</a></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<script>
				$(".button-collapse").sideNav();
			</script>
			<style>body{background-color:#e1f5fe;}</style>
			
			
				