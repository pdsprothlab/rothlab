<?php require 'header.php'?>
<title>Roth Lab - Publications</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(3).addClass("active");</script>
<h1>Recent Publications</h1>
<div class="row">
	<a target="_blank" href="//triggered.edina.clockss.org/ServeContent?url=http%3A%2F%2Fmolinterv.aspetjournals.org%2Fcontent%2F6%2F5%2F257.full"><img src="pictures/journals/molec_img.png" class='col s3 hoverable'></a>
	<a target="_blank" href="//www.nature.com/nchembio/journal/v7/n11/full/nchembio.662.html"><img class='col s3 hoverable' src="pictures/journals/ncb_d3.png"></a>
	<a target="_blank" href="//www.pnas.org/content/104/12/5163.full"><img class='col s3 hoverable' src="pictures/journals/pnas_img.png" ></a>
	<a target="_blank" href="//www.jbc.org/content/288/48/34470.full?sid=d8833a94-7e72-4a1d-a655-95a2b7451b2b"><img class='col s3 hoverable' src="pictures/journals/Roth_JBC_November29_cover.png" ></a>
	<a target="_blank" href="//www.nature.com/nature/journal/v485/n7398/full/nature11085.html"><img class='col s3 hoverable' src="pictures/journals/opiate.png" ></a>
	<a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25937170"><img class='col s3 hoverable' src="pictures/journals/neuron.png" ></a>
	<a target="_blank" href="//info.cell.com/best-of-neuron-2014-2015"><img class='col s3 hoverable' src="pictures/journals/neuron20142015.png" ></a>
	<a target="_blank" href="//www.cell.com/trends/pharmacological-sciences/issue?pii=S0165-6147(15)X0005-8"><img class='col s3 hoverable' src="pictures/journals/Trips2016.png" ></a>
    <a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/28129538"><img class='col s3 hoverable' src="pictures/journals/image001.png" ></a>
    <a target="_blank" href="https://www.nature.com/nsmb/volumes/26/issues/7"><img class='col s3 hoverable' src="pictures/journals/natureJuly2019.png" ></a>
    <a target="_blank" href="http://pharmrev.aspetjournals.org/content/71/4.cover-expansion"><img class='col s3 hoverable' src="pictures/journals/pharmacologicalOctober2019.jpg" ></a>
	<a target="_blank" href="https://ajp.psychiatryonline.org/toc/ajp/current"><img class='col s3 hoverable' src="pictures/journals/ajp_may2023.png" ></a>
</div>


<a class="btn btn-success btn-lg btn-group-justified" target="_blank" href="//www.ncbi.nlm.nih.gov/myncbi/browse/collection/44893413/?sort=date&direction=descending">Link to Rothlab Publications</a>
<style>
	.year{
	background-color:#bbdefb;
	color:white;
	font-size:24px;
	padding-left: 20px;
	padding-right: 20px;
	margin-bottom: 500px;
	margin-top: 500px;
	text-align:center;
	margin: 0 auto;
	display:block;
	}
	ul{
	margin-top:10px;
	}
	li {
		margin-top: 10px;
	}
</style>
<h1>Highlighted Recent Publications</h1>
<b class="year">2024</b>
<ol>
	<li>
		Kang HJ, Krumm BE, Tassou A, Geron M, DiBerto JF, Kapolka NJ, Gumpper RH, Sakamoto K, Dewran Kocak D, Olsen RHJ, Huang XP, Zhang S, Huang KL, Zaidi SA, Nguyen MT, Jo MJ, Katritch V, Fay JF, Scherrer G, <b> Roth BL </b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/39631393/">
			Structure-guided design of a peripherally restricted chemogenetic system
		</a>
		<b> Cell. 2024 </b> Nov 25:S0092-8674(24)01275-3. doi: 10.1016/j.cell.2024.11.001. Online ahead of print. PMID: 39631393
	</li>
	<li>
		Wang C, Liu Y, Lanier M, Yeager A, Singh I, Gumpper RH, Krumm BE, DeLeon C, Zhang S, Boehm M, Pittner R, Baron A, Dvorak L, Bacon C, Shoichet BK, Martinborough E, Fay JF, Cao C, <b> Roth BL </b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/39580805/">
			High-affinity agonists reveal recognition motifs for the MRGPRD GPCR
		</a>
		<b> Cell Rep. </b> 2024 Nov 23;43(12):114942. doi: 10.1016/j.celrep.2024.114942. Online ahead of print. PMID: 39580805 
	</li>
	<li>
		Chien DC, Limjunyawong N, Cao C, Meixiong J, Peng Q, Ho CY, Fay JF, <b> Roth BL </b>, Dong X.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/38718132/">
			MRGPRX4 mediates phospho-drug-associated pruritus in a humanized mouse model
		</a>
		<b> Sci Transl Med. </b> May 8;16(746):eadk8198. doi: 10.1126/scitranslmed.adk8198. Epub 2024 May 8. PMID: 38718132
	</li>
	<li>
	Kim Y, Gumpper RH, Liu Y, Kocak DD, Xiong Y, Cao C, Deng Z, Krumm BE, Jain MK, Zhang S, Jin J, <b> Roth BL </b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/38600377/">
			Bitter taste receptor activation by cholesterol and an intracellular tastant
		</a>
		<b> Nature. 2024 </b> Apr;628(8008):664-671. doi: 10.1038/s41586-024-07253-y. Epub 2024 Apr 10. PMID: 38600377

	</li>
	<li>
		Jiankun Lyu, Nicholas Kapolka, Ryan Gumpper, Assaf Alon, Liang Wang, Manish K. Jain, Ximena Barros-Álvarez, Kensuke Sakamoto, Yoojoong Kim, Jeffrey DiBerto, Kuglae Kim, Isabella S. Glenn, Tia A. Tummino, Sijie Huang, John J. Irwin, Olga O. Tarkhanova, Yurii Moroz, Georgios Skiniotis, Andrew C. Kruse, Brian K. Shoichet, <b>Bryan L. Roth</b>.
		<a target="_blank" href="https://www.science.org/doi/10.1126/science.adn6354">
			AlphaFold2 structures guide prospective ligand discovery
		</a>
		<b> Science </b> 2024 May Science0,eadn6354DOI:10.1126/science.adn6354
	</li>
	<li>
		Wang S, DeLeon C, Sun W, Quake SR, <b> Roth BL </b>, Südhof TC.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/38233523/">
			Alternative splicing of latrophilin-3 controls synapse formation
		</a>
		<b> Nature </b> 2024 Feb;626(7997):128-135. doi: 10.1038/s41586-023-06913-9. Epub 2024 Jan 17.
	</li>
	<li>
		Kocak DD, <b>Roth BL</b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/38182766/">
			Examining psychedelic drug action
		</a>
		<b>Nat Chem</b>. 2024 Jan;16(1):142. doi: 10.1038/s41557-023-01412-w.
	</li>
</ol>
<b class="year">2023</b>
<ol>
	<li>
		Che T, <b>Roth BL</b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/37995655/">
			Molecular basis of opioid receptor signaling
		</a>
		<b> Cell. </b> 2023 Nov 22;186(24):5203-5219. doi: 10.1016/j.cell.2023.10.029.
	</li>
	<li>
		Xu P, Huang S, Krumm BE, Zhuang Y, Mao C, Zhang Y, Wang Y, Huang XP, Liu YF, He X, Li H, Yin W, Jiang Y, Zhang Y, <b> Roth BL </b>, Xu HE.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/37221270/">
			Structural genomics of the human dopamine receptor system
		</a>
		<b> Cell Res. </b> 2023 Aug;33(8):604-616. doi: 10.1038/s41422-023-00808-0. Epub 2023 May 23.
	</li>
	<li>
		Krumm BE, DiBerto JF, Olsen RHJ, Kang HJ, Slocum ST, Zhang S, Strachan RT, Huang XP, Slosky LM, Pinkerton AB, Barak LS, Caron MG, Kenakin T, Fay JF, <b> Roth BL </b>.
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/36917754/">
			Neurotensin Receptor Allosterism Revealed in Complex with a Biased Allosteric Modulator
		</a>
		<b> Biochemistry. </b> 2023 Apr 4;62(7):1233-1248. doi: 10.1021/acs.biochem.3c00029. Epub 2023 Mar 14.
	</li>
	<li>
    <b>Bryan L Roth</b>, Ryan H Gumpper
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/37122272/">
	Psychedelics as Transformative Therapeutics
	</a>
	<b>The American Journal of Psychiatry.</b> 2023 May 1.
    </li>
	<li>
    Dongqi Zhang, Yongfeng Liu, Saheem A Zaidi, Lingyi Xu, Yuting Zhan, Anqi Chen, Jiangtao Guo,
	Xi-Ping Huang, <b>Bryan L Roth</b>, Vsevolod Katritch, Vadim Cherezov, Haitao Zhang
    <a target="_blank" href="https://www.embopress.org/doi/full/10.15252/embj.2022112940">
	Structural insights into angiotensin receptor signaling modulation by balanced and biased agonists
	</a>
	<b>The EMBO Journal.</b> 2023 Apr 11.
    </li>
	<li>
    Brian E. Krumm, Jeffrey F. DiBerto, Reid H. J. Olsen, Hye Jin Kang, Samuel T. Slocum,
	Shicheng Zhang, Ryan T. Strachan, Xi-Ping Huang, Lauren M. Slosky, Anthony B. Pinkerton,
	Lawrence S. Barak, Marc G. Caron, Terry Kenakin*, Jonathan F. Fay, and <b>Bryan L. Roth</b>.
    <a target="_blank" href="https://pubs.acs.org/doi/full/10.1021/acs.biochem.3c00029">
	Neurotensin Receptor Allosterism Revealed in Complex with a Biased Allosteric Modulator
	</a>
	<b>ACS.</b> 2023 Mar 14.
    </li>
	<li>
    Yue Wang, Youwen Zhuang, Jeffrey F DiBerto, X Edward Zhou, Gavin P Schmitz, Qingning Yuan, Manish K Jain, Weiyi Liu,
	Karsten Melcher, Yi Jiang, <b>Bryan L. Roth</b>, H Eric Xu.
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/36638794/">
	Structures of the entire human opioid receptor family.
	</a>
	<b>Cell.</b> 2023 Jan 11;S0092-8674(22)01545-8. doi: 10.1016/j.cell.2022.12.026. Online ahead of print.
    </li>
	<li>
    Ryan H.Gumpper, <b>Bryan L. Roth</b>.
    <a target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0092867422015331?via%3Dihub">
	SnapShot: Psychedelics and serotonin receptor signaling
	</a>
	<b>Cell.</b> 2023 Jan 5.
    </li>
</ol>
<b class="year">2022</b>
<ol>
	<li>
		Shicheng Zhang, Ryan H. Gumpper, Xi-Ping Huang, Yongfeng Liu, Brian E. Krumm, Can Cao, Jonathan F. Fay & <b> Bryan L. Roth. </b>
		<a target="_blank" href="https://www.nature.com/articles/s41586-022-05489-0">Molecular basis for selective activation of DREADD-based chemogenetics</a>
		<b>Nature </b> 612, 354–362 (2022). https://doi.org/10.1038/s41586-022-05489-0
    </li>
	<li>
		Yongfeng Liu, Can Cao, Xi-Ping Huang, Ryan H Gumpper, Moira M Rachman, Sheng-Luen Shih, Brian E Krumm, Shicheng Zhang, 
		Brian K Shoichet, Jonathan F Fay, <b>Bryan L. Roth</b>
		<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/36302898/">Ligand recognition and allosteric modulation of the human MRGPRX1 receptor</a>
		<b>Nat Chem Biol.</b> 2022 Oct 27. doi: 10.1038/s41589-022-01173-6. Online ahead of print.
    </li>
	<li>
    Alex C. Kwan, David E. Olson, Katrin H. Preller & <b>Bryan L. Roth.</b>
    <a target="_blank" href="https://www.nature.com/articles/s41593-022-01177-4">
		The neural basis of psychedelic action.
	</a>
	<b>Nature Neuroscience.</b> 24 October 2022.
    </li>
	<li>
    Can Cao, Ximena Barros-Álvarez, Shicheng Zhang, Kuglae Kim, Marc A Dämgen, Ouliana Panova 2, 
	Carl-Mikael Suomivuori, Jonathan F Fay, Xiaofang Zhong, Brian E Krumm, Ryan H Gumpper, 
	Alpay B Seven, Michael J Robertson, Nevan J Krogan, Ruth Hüttenhain, David E Nichols, Ron O Dror, 
	Georgios Skiniotis, <b>Bryan L. Roth</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/36087581/">Signaling snapshots of a serotonin receptor activated by the prototypical psychedelic LSD</a>
	<b>Neuron.</b> 2022 Oct 5;110(19):3154-3167.e7. doi: 10.1016/j.neuron.2022.08.006. Epub 2022 Sep 9.
    </li>
	<li>
    Anat Levit Kaplan, Danielle N Confair, Kuglae Kim, Ximena Barros-Álvarez, Ramona M Rodriguiz, Ying Yang, Oh Sang Kweon, 
	Tao Che, John D McCorvy, David N Kamber, James P Phelan, Luan Carvalho Martins, Vladimir M Pogorelov, Jeffrey F DiBerto, 
	Samuel T Slocum, Xi-Ping Huang, Jain Manish Kumar, Michael J Robertson, Ouliana Panova, Alpay B Seven, Autumn Q Wetsel, 
	William C Wetsel, John J Irwin, Georgios Skiniotis, Brian K Shoichet, <b>Bryan L Roth</b>, Jonathan A Ellman
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/36171289/">Bespoke library docking for 5-HT2A receptor agonists with antidepressant activity</a>
	<b>Nature.</b> 2022 Sep 28. doi: 10.1038/s41586-022-05258-z. Online ahead of print.
    </li>
	<li>
    Gavin P Schmitz, Manish K Jain, Samuel T Slocum, <b>Bryan L. Roth.</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/35894503/">
	5-HT2A SNPs Alter the Pharmacological Signaling of Potentially Therapeutic Psychedelics
	</a>
	<b>ACS Chem Neurosci.</b> 2022 Aug 17;13(16):2386-2398. doi: 10.1021/acschemneuro.1c00815. Epub 2022 Jul 27.
    </li>
	<li>
    Ryan H Gumpper, Jonathan F Fay, <b>Bryan L. Roth</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/35977511/">Molecular insights into the regulation of constitutive activity by RNA editing of 5HT2C serotonin receptors</a>
	<b>Cell Rep.</b> 2022 Aug 16;40(7):111211. doi: 10.1016/j.celrep.2022.111211.
    </li>
    <li>
    Shicheng Zhang, He Chen, Chengwei Zhang, Ying Yang, Petr Popov, Jing Liu, Brian E Krumm, Can Cao, Kuglae Kim, Yan Xiong, Vsevolod Katritch, 
	Brian K Shoichet, Jian Jin, Jonathan F Fay, <b>Bryan L. Roth</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/35835867/">Inactive and active state structures template selective tools for the human 5-HT 5A receptor</a>
	<b>Nat Struct Mol Biol.</b> 2022 Jul;29(7):677-687. doi: 10.1038/s41594-022-00796-6. Epub 2022 Jul 14. 
    </li>
    <li>
    Tristan D. McClure-Begley &amp; <b>Bryan L. Roth</b>
    <a target="_blank" href="https://www.nature.com/articles/s41573-022-00421-7">The promises and perils of psychedelic pharmacology for psychiatry</a>
	<b> Nature Reviews.</b> Drug Discovery 2022 March 17.
    </li>
	<li>
	Arman A Sadybekov, Anastasiia V Sadybekov, Yongfeng Liu, Christos Iliopoulos-Tsoutsouvas, Xi-Ping Huang, Julie Pickett,
	Blake Houser, Nilkanth Patel, Ngan K Tran, Fei Tong, Nikolai Zvonok, Manish K Jain, Olena Savych, Dmytro S Radchenko,
	Spyros P Nikas, Nicos A Petasis, Yurii S Moroz, <b>Bryan L Roth</b>, Alexandros Makriyannis, Vsevolod Katritch
	<a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/34912117/">Synthon-based ligand discovery in virtual libraries of over 11 billion compounds</a>
	<b> Nature.</b> 2022 January;601(7893):452-459.doi: 10.1038/s41586-021-04220-9. Epub 2021 Dec 15. 
    </li>
</ol>
<b class="year">2021</b>
<ol>
    <li>
    Can Cao, Hye Jin Kang, Isha Singh, He Chen, Chengwei Zhang, Wenlei Ye, Byron W. Hayes, Jing Liu, Ryan H. Gumpper, Brian J. Bender, 
	Samuel T. Slocum, Brian E. Krumm, Katherine Lansu, John D. McCorvy, Wesley K. Kroeze, Justin G. English, Jeffrey F. DiBerto, 
	Reid H. J. Olsen, Xi-Ping Huang, Shicheng Zhang, Yongfeng Liu, Kuglae Kim, Joel Karpiak, Lily Y. Jan, Soman N. Abraham, Jian Jin, 
	Brian K. Shoichet, Jonathan F. Fay & <b>Bryan L. Roth</b>
    <a target="_blank" href="https://www.nature.com/articles/s41586-021-04126-6">Structure, function and pharmacology of human itch GPCRs</a>
	<b>Nature.</b> 2021 November 17.
    </li>
    
    <li>
    Tao Che, Hemlata Dwivedi-Agnihotri, Arun K Shukla, <b>Bryan L Roth</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/33824179/">Biased ligands at opioid receptors: Current status and future directions</a>
	<b> Sci Signal.</b> 2021 Apr 6;14(677):eaav0320. doi: 10.1126/scisignal.aav0320.
    </li>
    <li>
    Tao Che, <b>Bryan L Roth</b>
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/33756098/">Structural Insights Accelerate the Discovery of Opioid Alternatives</a>
	<b>Annu Rev Biochem.</b> 2021 Mar 23. doi: 10.1146/annurev-biochem-061620-044044. Online ahead of print. 
    </li>

    <li>
    Zhuang Y, Xu P, Mao C, Wang L, Krumm B, Zhou XE, Huang S, Liu H, Cheng X, Huang XP, Shen DD, Xu T, Liu YF, Wang Y, Guo J, Jiang Y, 
	Jiang H, Melcher K, <b>Roth BL</b>, Zhang Y, Zhang C, Xu HE.
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/33571431/">Structural insights into the human D1 and D2 dopamine receptor signaling complexes</a>
	<b> Cell.</b> 2021 Feb 18;184(4):931-942.e18. doi: 10.1016/j.cell.2021.01.027. Epub 2021 Feb 10.  
    </li>

    <li>
    Uprety R, Che T, Zaidi SA, Grinnell SG, Varga BR, Faouzi A, Slocum ST, Allaoa A, Varadi A, Nelson M, Bernhard SM, Kulko E, Le Rouzic V, 
	Eans SO, Simons CA, Hunkele A, Subrath J, Pan YX, Javitch JA, McLaughlin JP, <b>Roth BL</b>, Pasternak GW, Katritch V, Majumdar S.
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/33555255/">Controlling opioid receptor functional selectivity by targeting distinct subpockets of the orthosteric site</a>
	<b> Elife.</b> 2021 Feb 8;10:e56519. doi: 10.7554/eLife.56519. 
    </li>

    <li>
    Peiyu Xu, Sijie Huang, Chunyou Mao, Brian E Krumm, X Edward Zhou, Yangxia Tan, Xi-Ping Huang, 
	Yongfeng Liu, Dan-Dan Shen, Yi Jiang, Xuekui Yu, Hualiang Jiang, Karsten Melcher, <b>Bryan L Roth</b>, Xi Cheng, Yan Zhang, H Eric Xu
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/33548201/">Structures of the human dopamine D3 receptor-Gi complexes.</a>
	<b> Mol Cell.</b> 2021 Jan 27:S1097-2765(21)00003-4. doi: 10.1016/j.molcel.2021.01.003.
    </li>
</ol>

<b class="year">2020</b>

<ol>

    <li>Kuglae Kim, Tao Che, Ouliana Panova, Jeffrey F DiBerto, Jiankun Lyu, Brian E Krumm,
	 Daniel Wacker, Michael J Robertson, Alpay B Seven, David E Nichols, Brian K Shoichet, 
	 Georgios Skiniotis, <b>Roth BL</b> 
    <a target="_blank" href="https://pubmed.ncbi.nlm.nih.gov/32946782/">Structure of a Hallucinogen-Activated Gq-Coupled 5-HT 2A Serotonin Receptor</a>
	<b> Cell.</b> 2020 Sep 17;182(6):1574-1588.e19. doi: 10.1016/j.cell.2020.08.024.
    </li>

    <li>Pottel J, Armstrong D, Zou L, Fekete A, Huang XP, Torosyan H, Bednarczyk D, Whitebread S, Bhhatarai B, Liang G, Jin H, Ghaemi SN, Slocum S, 
	Lukacs KV, Irwin JJ, Berg EL, Giacomini KM, <b>Roth BL</b>, Shoichet BK, Urban L 
    <a target="_blank" href="https://pmlegacy.ncbi.nlm.nih.gov/pubmed/32703874">The activities of drug inactive ingredients on biological targets.</a>
	<b> Science.</b>. 2020 Jul 24;369(6502):403-413. doi: 10.1126/science.aaz9906. 
    </li>

    <li>Nagai Y, Miyakawa N, Takuwa H1, Hori Y, Oyama K, Ji B, Takahashi M, Huang XP2, Slocum ST, DiBerto JF, Xiong Y, Urushihata T, Hirabayashi T, Fujimoto A, 
	Mimura K, English JG, Liu J, Inoue KI, Kumata K, Seki C, Ono M, Shimojo M, Zhang MR, Tomita Y, Nakahara J, Suhara T, Takada M, Higuchi M, Jin J, <b>Roth BL</b>, 
	Minamimoto T. 
    <a target="_blank" href="https://pmlegacy.ncbi.nlm.nih.gov/pubmed/32632286">Deschloroclozapine, a potent and selective chemogenetic actuator enables rapid neuronal and behavioral modulations in mice and monkeys.</a>
	<b> Nature Neuroscience.</b>. 2020 Jul 6. doi: 10.1038/s41593-020-0661-3. [Epub ahead of print]
    </li>


	<li>Reid H. J. Olsen, Jeffrey F. DiBerto, Justin G. English, Alexis M. Glaudin, Brian E. Krumm, Samuel T. Slocum, Tao Che, Ariana C. Gavin, John D. McCorvy, <b>Bryan L. Roth</b> & Ryan T. Strachan 
    <a target="_blank" href="https://www.nature.com/articles/s41589-020-0535-8">TRUPATH, an open-source biosensor platform for interrogating the GPCR transducerome</a><b> Nature Chemical Biology.</b>. 2020 May 04. DOI: https://doi.org/10.1038/s41589-020-0535-8
    </li>

	<li>David E. Gordon, Gwendolyn M. Jang, Mehdi Bouhaddou, Jiewei Xu, Kirsten Obernier, Kris M. White, Matthew J. O’Meara, Veronica V. Rezelj, Jeffrey Z. Guo, 
		Danielle L. Swaney, Tia A. Tummino, Ruth Huettenhain, Robyn M. Kaake, Alicia L. Richards, Beril Tutuncuoglu, Helene Foussard, Jyoti Batra, Kelsey Haas, 
		Maya Modak, Minkyu Kim, Paige Haas, Benjamin J. Polacco, Hannes Braberg, Jacqueline M. Fabius, Manon Eckhardt, Margaret Soucheray, Melanie J. Bennett, 
		Merve Cakir, Michael J. McGregor, Qiongyu Li, Bjoern Meyer, Ferdinand Roesch, Thomas Vallet, Alice Mac Kain, Lisa Miorin, Elena Moreno, Zun Zar Chi Naing, 
		Yuan Zhou, Shiming Peng, Ying Shi, Ziyang Zhang, Wenqi Shen, Ilsa T. Kirby, James E. Melnyk, John S. Chorba, Kevin Lou, Shizhong A. Dai, Inigo Barrio-Hernandez, 
		Danish Memon, Claudia Hernandez-Armenta, Jiankun Lyu, Christopher J. P. Mathy, Tina Perica, Kala B. Pilla, Sai J. Ganesan, Daniel J. Saltzberg, Ramachandran Rakesh, 
		Xi Liu, Sara B. Rosenthal, Lorenzo Calviello, Srivats Venkataramanan, Jose Liboy-Lugo, Yizhu Lin, Xi-Ping Huang, YongFeng Liu, Stephanie A. Wankowicz, Markus Bohn, 
		Maliheh Safari, Fatima S. Ugur, Cassandra Koh, Nastaran Sadat Savar, Quang Dinh Tran, Djoshkun Shengjuler, Sabrina J Fletcher, Michael C. O’Neal, Yiming Cai, 
		Jason C. J. Chang, David J. Broadhurst, Saker Klippsten, Phillip P. Sharp, Nicole A. Wenzell, Duygu Kuzuoglu, Hao-Yuan Wang, Raphael Trenker, Janet M. Young, Devin A. Cavero, 
		Joseph Hiatt, Theodore L. Roth, Ujjwal Rathore, Advait Subramanian, Julia Noack, Mathieu Hubert, Robert M. Stroud, Alan D. Frankel, Oren S. Rosenberg, Kliment A Verba, David A. Agard, 
		Melanie Ott, Michael Emerman, Natalia Jura, Mark von Zastrow, Eric Verdin, Alan Ashworth, Olivier Schwartz, Christophe d’Enfert, Shaeri Mukherjee, Matt Jacobson, Harmit S. Malik, 
		Danica G. Fujimori, Trey Ideker, Charles S. Craik, Stephen N. Floor, James S. Fraser, John D. Gross, Andrej Sali, <b>Bryan L. Roth</b>, Davide Ruggero, Jack Taunton, Tanja Kortemme, 
		Pedro Beltrao, Marco Vignuzzi, Adolfo García-Sastre, Kevan M. Shokat, Brian K. Shoichet & Nevan J. Krogan
    <a target="_blank" href="https://www.nature.com/articles/s41586-020-2286-9">A SARS-CoV-2 protein interaction map reveals targets for drug repurposing</a>
	<b> Nature.</b>. 2020 April 30. DOI: https://doi.org/10.1038/s41586-020-2286-9
    </li>

	<li>Brian Krumm, <b>Bryan L. Roth</b>
    <a target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0969212620300459">A Structural Understanding of Class B GPCR Selectivity and Activation Revealed</a><b> Structure</b>. 2020 March 03. DOI: https://doi.org/10.1016/j.str.2020.02.004
    </li>
    
	<li>Tao Che, Hustin English, Brian E. Krumm, Kuglae Kim, Els Pardon, Reid H. J. Olsen, Sheng Wang, Shicheng Zhang, Jeffrey F. DiBerto, Noah Sciaky, F Ivy Carroll, Jan Steyaert, Daniel Wacker & <b>Bryan L. Roth</b>
    <a target="_blank" href="https://www.nature.com/articles/s41467-020-14889-7">Nanobody-enabled monitoring of kappa opioid receptor states</a><b> Nature Communications.</b>. 2020 March 02. DOI: https://doi.org/10.1038/s41467-020-14889-7
    </li>
	
	<li>Brian Krumm, <b>Bryan L. Roth*</b>
    <a target="_blank" href="https://www.nature.com/articles/d41586-020-00411-y">A self-activating orphan receptor</a><b> Nature.</b>. 2020 February 19. DOI: 10.1038/d41586-020-00411-y
    </li>

	<li>Reed M. Stein, Hye Jin Kang, John D. McCorvy, Grant C. Glatfelter, Anthony J. Jones, Tao Che, Samuel Slocum, Xi-Ping Huang, Olena Savych, Yurii S. Moroz, Benjamin Stauch, Linda C. Johansson, Vadim Cherezov, Terry Kenakin, John J. Irwin, Brian K. Shoicet*, <b>Bryan L. Roth*</b>, Margarita L. Dubocovich*
    <a target="_blank" href="https://www.nature.com/articles/s41586-020-2027-0_reference.pdf">Virtual discovery of melatonin receptor ligands to modulate circadian rythms</a><b> Nature.</b>. 2020 February 10. DOI: https://doi.org/10.1038/s41586-020-2027-0
    <br>*=Co-corresponding authors
    </li>

</ol>

<b class="year">2019</b>

<ol>
<li>Simon R. Foster, Alexander S. Hauser, Line Vedel, ..., <b>Bryan L. Roth</b>, Hans Br&auml;uner-Osborne, David E. Gloriam
<a target="_blank" href="https://www.cell.com/cell/fulltext/S0092-8674(19)31126-2">Discovery of Human Signaling Systems: Pairing Peptides to G Protein-Coupled Receptors</a><b> Cell</b>. 2019 October 31. DOI: https://doi.org/10.1016/j.cell.2019.10.010
</li>

<li>Justin G. English, Reid H.J. Olsen, Katherine Lansu, M. Patel, K. White, A.S. Cockrell, D. Singh, Ryan T. Strachan, Daniel Wacker, <b>Bryan L. Roth</b>
<a target="_blank" href="https://www.cell.com/cell/fulltext/S0092-8674(19)30622-1"> VEGAS as a Platform for Facile Directed Evolution in Mammalian Cells</a><b> Cell.</b>. 2019 July 4. DOI:https://doi.org/10.1016/j.cell.2019.05.051
</li>

<li><b>Bryan L. Roth</b>
<a target="blank" href="https://www.ncbi.nlm.nih.gov/pubmed/31270468"> Molecular pharmacology of metabotropic receptors targeted by neuropsychiatric drugs.</a><b> Nat Struct Mol Biol.</b> 2019 July 3; 26(7):535-544. doi: 10.1038/s41594-019-0252-8. Epub 2019 Jul 3.
</li>

<li>Stauch B, Johansson LC, McCorvy JD, Patel N, Han GW, Huang XP, Gati C, Batyuk A, Slocum ST, Ishchenko A, Brehm W, White TA, Michaelian N, Madsen C, Zhu L, Grant TD, Grandner JM, Shiriaeva A, Olsen RHJ, Tribo AR, Yous S, Stevens RC, Weierstall U, Katritch V, *<b>Roth BL</b>, *Liu W, *Cherezov V.
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/31019306"> Structural basis of ligand recognition at the human MT1 melatonin receptor.</a><b> Nature.</b>. 2019 Apr 24. doi: 10.1038/s41586-019-1141-3. [Epub ahead of print]
<br>
*BLR, WL and VC corresponding authors.

</li>
<li>Johansson LC, Stauch B, McCorvy JD, Han GW, Patel N, Huang XP, Batyuk A, Gati C, Slocum ST, Li C, Grandner JM, Hao S, Olsen RHJ, Tribo AR, Zaare S, Zhu L, Zatsepin NA, Weierstall U, Yous S, Stevens RC, Liu W, *<b>Roth BL</b>, *Katritch V, *Cherezov V.
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/31019305"> XFEL structures of the human MT2 melatonin receptor reveal the basis of subtype selectivity.</a><b> Nature.</b>. 2019 Apr 24. doi: 10.1038/s41586-019-1144-0. [Epub ahead of print]
<br>
*BLR, VK and VC corresponding authors.

</li>

<li><b>Bryan L. Roth</b>
<a target="_blank" href="https://www.sciencedirect.com/science/article/pii/S0959440X18301222">How structure informs and transforms chemogenetics</a><b> Current Opinion in Structural Biology</b> February 25, 2019 https://doi.org/10.1016/j.sbi.2019.01.016
</li>

<li>Julia Kozlitina , Davide Risso , Katherine Lansu, Reid Hans Johnson Olsen, Eduardo Sainz, Donata Luiselli, Arnab Barik, Carlos Frigerio-Domingues, Luca Pagani, Stephen Wooding, Thomas Kirchner, Ray Niaura, <b>Bryan Roth</b>, Dennis Drayna
<a target="_blank" href="https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1007916">An African-specific haplotype in MRGPRX4 is associated with menthol cigarette smoking</a><b> PLOS Genetics</b> February 15, 2019 https://doi.org/10.1371/journal.pgen.1007916
</li>

<li>Jiankun Lyu, Sheng Wang, Trent E. Balius, Isha Singh, Anat Levit, Yurii S. Moroz, Matthew J. O’Meara, Tao Che, Enkhjargal Algaa, Kateryna Tolmachova, Andrey A. Tolmachev, *Brian K. Shoichet, <b>*Bryan L. Roth</b> and *John J. Irwin
<a target="_blank" href="https://www.nature.com/articles/s41586-019-0917-9">Ultra-large library docking for discovering new chemotypes </a><b>Nature</b> 14 February 2019 https://doi.org/10.1038/s41586-019-0917-9
<br>*=BKS, BLR, and JJI=co-corresponding authors
</li>
</ol>

<b class="year">2018</b>
<ol>
<li>John D. McCorvy, Daniel Wacker, Sheng Wang, Bemnat Agegnehu, Jing Liu, Katherine Lansu, Alexandra R. Tribo, Reid H. J. Olsen, Tao Che, Jian Jin & <b> Bryan L. Roth </b>
<a target="_blank" href="https://www.nature.com/articles/s41594-018-0116-7">Structural determinants of 5-HT2B receptor activation and biased agonism</a> <b>Nat Struct Mol Biol.</b> 20 August 2018
</li>

<li>
Brian Krumm & <b>Bryan L. Roth</b>
<a target="_blank" href="https://www.nature.com/articles/d41586-018-04977-6">Arresting vistas of arrestin activation</a> <b>Nature.</b> 02 May 2018; DOI: https://doi.org/10.1038/d41586-018-04977-6
</li>

<li>
Yao Peng, John D. McCorvy, Kasper Harpsøe, Katherine Lansu, Shuguang Yuan, Petr Popov, Lu Qu, Mengchen Pu, Tao Che, Louise F. Nikolajsen, Xi-Ping Huang, Yiran Wu, Ling Shen, Walden E. Bjørn-Yoshimoto, Kang Ding, Daniel Wacker, Gye Won Han, Jianjun Cheng, Vsevolod Katritch, Anders A. Jensen, Michael A. Hanson, Suwen Zhao, David E. Gloriam, <b>Bryan L. Roth</b>, Raymond C. Stevens, Zhi-Jie Liu
<a target="_blank" href="http://www.cell.com/cell/fulltext/S0092-8674(18)30001-1">5-HT2C Receptor Structures Reveal the Structural Basis of GPCR Polypharmacology</a> <b>Cell.</b> 2018; DOI: https://doi.org/10.1016/j.cell.2018.01.001
</li>
<li>
Sheng Wang, Tao Che, Anat Levit, Brian K. Shoichet, Daniel Wacker & <b>Bryan L. Roth</b><br>
<a target="_blank" href="https://www.nature.com/articles/nature25758">Structure of the D2 dopamine receptor bound to the atypical antipsychotic drug risperidone.</a> <b>Nature.</b> 2018; DOI: 10.1038/nature25758
</li>
<li>
Tao Che, Susruta Majumdar, Saheem A. Zaidi, Pauline Ondachi, John D. McCorvy, Sheng Wang, Philip D. Mosier, Rajendra Uprety, Eyal Vardy, Brian E. Krumm, Gye Won Han, Ming-Yue Lee, Els Pardon, Jan Steyaert, Xi-Ping Huang, Ryan T. Strachan, Alexandra R. Tribo, Gavril W. Pasternak, F. Ivy Carroll, Raymond C. Stevens, Vadim Cherezov, Vsevolod Katritch, Daniel Wacker, <b>Bryan L. Roth</b>.<br>
<a target="_blank" href="http://www.cell.com/cell/fulltext/S0092-8674(17)31491-5">Structure of the Nanobody-Stabilized Active State of the Kappa Opioid Receptor</a> <b>Cell.</b> DOI: http://dx.doi.org/10.1016/j.cell.2017.12.011
	</li>
</ol>

<b class="year">2017</b>

<ol>
<li>
McCorvy JD, Butler KV, Kelly B, Rechsteiner K, Karpiak J, Betz RM, Kormos BL, Shoichet BK, Dror RO, Jin J, <b>Roth BL</b>.<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/29227473">Structure-inspired design of β-arrestin-biased ligands for aminergic GPCRs.</a> <b>Nat Chem Biol.</b> 2017 Dec 11.doi: 10.1038/nchembio.2527. [Epub ahead of print]
      </li>
<li>
<b>Roth BL.</b>, Irwin JJ, Shoichet BK.<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/29045379">Discovery of new GPCR ligands to illuminate new biology.</a> <b>Nat Chem Biol.</b> 2017 Nov; 13(11):1143-1151. doi: 10.1038/nchembio.2490. Epub 2017 Oct 18.
      </li>
<li>Wang S, Wacker D, Levit A, Che T, Betz RM, McCorvy JD, Venkatakrishnan AJ, Huang XP, Dror RO, Shoichet BK, <b>Roth BL.</b><br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/29051383">D4 dopamine receptor high-resolution structures enable the discovery of selective agonists.</a> <b>Science.</b> 2017 Oct 20;358(6361):381-386. doi: 10.1126/science.aan5468.
      </li>
      

       <li>Wacker D, Stevens RC, Roth BL.<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/28753422">How Ligands Illuminate GPCR Molecular Pharmacology.</a> <b>Cell</b> 22017 Jul 27;170(3):414-427. doi: 10.1016/j.cell.2017.07.009.
      </li>

       <li>Lansu K<sup>a</sup>, Karpiak J<sup>a</sup>, Liu J, Huang XP, McCorvy JD, Kroeze WK, Che T, Nagase H, Carroll FI, Jin J, Shoichet BK, Roth BL.<br>
<sup>a</sup> Co-first authors<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/28288109">In silico design of novel probes for the atypical opioid receptor MRGPRX2.</a> <b>Nat Chem Biol.</b> 2017 Mar 13. doi: 10.1038/nchembio.2334. [Epub ahead of print]
      </li>


       <li>Daniel Wacker<sup>ab</sup>, Sheng Wang<sup>a</sup>, John D. McCorvy<sup>a</sup>, Robin M. Betz, A.J. Venkatakrishnan, Anat Levit, Katherine Lansu, Zachary L. Schools, Tao Che, David E. Nichols, Brian K. Shoichet, Ron O. Dror<sup>b</sup>, Bryan L. Roth<sup>bc</sup><br>
<sup>a</sup> Co-first authors<br>
<sup>b</sup> Co-corresonding<br>
<sup>c</sup> Lead Contact<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/28129538">Crystal Structure of an LSD-Bound Human Serotonin Receptor.</a> <b>Cell </b> 2017 Jan 26;168(3):377-389.e12. doi: 10.1016/j.cell.2016.12.033.
	</li>
</ol>
<b class="year">2016</b>
<ol>
       <li>Kim Y, Lee HM, Xiong Y, Sciaky N, Hulbert SW, Cao X, Everitt JI, Jin J, Roth BL*, Jiang YH*<br>
*Co-corresonding authors<br>
<a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/28024084">Targeting the histone methyltransferase G9a activates imprinted genes and improves survival of a mouse model of Prader-Willi syndrome.</a> <b>Nature. </b>Nat Med. 2016 Dec 26. doi: 10.1038/nm.4257. [Epub ahead of print]
	</li>

        <li>Aashish Manglik, Henry Lin, Dipendra K. Aryal, John D. McCorvy, Daniela Dengler, Gregory Corder,	Anat Levit, Ralf C. Kling, Viachaslau Bernat, Harald Hübner, Xi-Ping Huang, Maria F. Sassano, Patrick M. Giguère, Stefan Löber, Da Duan,Grégory Scherrer, Brian K. Kobilka*, Peter Gmeiner*, Bryan L. Roth* & Brian K. Shoichet*<br>
* Co-corresponding authors<br>
<a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/?term=shoichet+and+opioid">Structure-based discovery of opioid analgesics with reduced side effects.</a> <b>Nature.</b> 2016 Aug 17:1-6. doi: 10.1038/nature19112. [Epub ahead of print]
	</li>

        <li>Rennekamp AJ, Huang XP, Wang Y, Patel S, Lorello PJ, Cade L, Gonzales AP, Yeh JJ, Caldarone BJ,Roth BL, Kokel D, Peterson RT. <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/27239788">σ1 receptor ligands control a switch between passive and active threat responses. </a> <b>Nat Chem Biol. </b> 2016 May 30. doi: 10.1038/nchembio.2089. [Epub ahead of print]</li>

        <li>Bruni G, Rennekamp AJ, Velenich A, McCarroll M, Gendelev L, Fertsch E, Taylor J, Lakhani P, Lensen D, Evron T, Lorello PJ, Huang XP, Kolczewski S, Carey G, Caldarone BJ, Prinssen E, Roth BL, Keiser MJ, Peterson RT, Kokel D. <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/27239787">Zebrafish behavioral profiling identifies multitarget antipsychotic-like compounds. </a> <b>Nature Chem Biol. </b> 2016 May 30. doi: 10.1038/nchembio.2097. [Epub ahead of print]</li>

	<li>Wacker D and <b>Roth BL</b> <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/27045444">An alerting structure: human orexin receptor 1.</a> <b>Nat Struct Mol Biol.</b> 23(4):265-266. doi: 10.1038/nsmb.3198, 2016 Apr 5</li>
	<li><b>Roth BL</b> <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/26889809">DREADDs for Neuroscientists.</a> <b>Neuron</b> 89(4):683-694. doi:10.1016/j.neuron, 2016 Feb 17</li>
	<li>MR Bruchas and <b>BL Roth</b> <a target="_blank" href="http://www.sciencedirect.com/science/article/pii/S016561471600002X">New technologies for elucidating opioid receptor function. </a> <b>Trends in Pharmacological Sciences.</b> doi:10.1016/j.tips.2016.01.001, 2016 Jan 29</li>
	<li>Stuckey JI, Dickson BM, Cheng N, Liu Y, Norris JL, Cholensky SH, Tempel W, Qin S, Huber KG, Sagum C, Black K, Li F, Huang XP, <b>Roth BL</b>, Baughman BM, Senisterra G, Pattenden SG, Vedadi M, Brown PJ, Bedford MT, Min J, Arrowsmith CH, James LI, Frye SV. <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/26807715">A cellular chemical probe targeting the chromodomains of Polycomb repressive complex 1.</a> <b>Nat Chem Biol.</b> 10.1038/nchembio.2007, 2016 Jan 25</li>
</ol>
<b class="year">2015</b>
<ol>
	<li>Xi-Ping Huang, Joel Karpiak, Wesley K. Kroeze, Hu Zhu, Xin Chen, Sheryl S. Moy, Kara A. Saddoris, Viktoriya D. Nikolova, Martilias S. Farrell, Sheng Wang, Thomas J. Mangano, Deepak A. Deshpande, Alice Jiang, Raymond B. Penn, Jian Jin, Beverly H. Koller, Terry Kenakin, Brian K. Shoichet & Bryan L. Roth.<a target="_blank" href="http://www.nature.com/nature/journal/vaop/ncurrent/full/nature15699.html">Allosteric ligands for the pharmacologically dark receptors GPR68 and GPR65</a> <b>Nature</b> doi:10.1038/nature15699, 2015 Nov 09</li>
	<li>Elkins JM, Fedele V, Szklarz M, Abdul Azeez KR, Salah E, Mikolajczyk J, Romanov S, Sepetov N, Huang XP, Roth BL, Al Haj Zen A, Fourches D, Muratov E, Tropsha A, Morris J, Teicher BA, Kunkel M, Polley E, Lackey KE, Atkinson FL, Overington JP, Bamborough P, Müller S, Price DJ, Willson TM, Drewry DH, Knapp S, Zuercher WJ.<a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/26501955">Comprehensive characterization of the Published Kinase Inhibitor Set.</a> <b>Nature Biotechnol</b>,  doi: 10.1038/nbt.3374, 2015 Oct 26</li>
	<li>Arrowsmith CH, Audia JE, Austin C, Baell J, Bennett J, Blagg J, Bountra C, Brennan PE, Brown PJ, Bunnage ME, Buser-Doepner C, Campbell RM, Carter AJ, Cohen P, Copeland RA, Cravatt B, Dahlin JL, Dhanak D, Edwards AM, Frederiksen M, Frye SV, Gray N, Grimshaw CE, Hepworth D, Howe T, Huber KV, Jin J, Knapp S, Kotz JD, Kruger RG, Lowe D, Mader MM, Marsden B, Mueller-Fahrnow A, Müller S, O'Hagan RC, Overington JP, Owen DR, Rosenberg SH, Roth BL, Ross R, Schapira M, Schreiber SL, Shoichet B, Sundström M, Superti-Furga G, Taunton J, Toledo-Sherman L, Walpole C, Walters MA, Willson TM, Workman P, Young RN, Zuercher WJ.</a>
	<a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/26196764">The promise and peril of chemical probes.</a> <b>Nature Chem Biol.</b>, 11(8):536-41. doi: 10.1038/nchembio.1867, 2015 Aug.</li>
	<li>Vardy E, Robinson JE, Li C, Olsen RH, DiBerto JF, Giguere PM, Sassano FM, Huang XP, Zhu H, Urban DJ, White KL, Rittiner JE, Crowley NA, Pleil KE, Mazzone CM, Mosier PD, Song J, Kash TL, Malanga CJ, Krashes MJ, Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25937170">A New DREADD facilitates the multiplexed chemogenetic interrogation of behavior.</a>  <b>Neuron</b>, 86: 936-46, 2015.</li>
	<li>Kroeze WK, Sassano MF, Huang XP, Lansu K, McCorvy JD, Giguère PM, Sciaky N, Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25895059">PRESTO-Tango as an open-source resource for interrogation of the druggable human GPCRome.</a> <b>Nature Struct Mol Biol</b>. 22: 362-369, 2015.</li>
	<li>Fenalti G, Zatsepin NA, Betti C, Giguere P, Han GW, Ishchenko A, Liu W, Guillemyn K, Zhang H, James D, Wang D, Weierstall U, Spence JC, Boutet S, Messerschmidt M, Williams GJ, Gati C, Yefanov OM, White TA, Oberthuer D, Metz M, Yoon CH, Barty A, Chapman HN, Basu S, Coe J, Conrad CE, Fromme R, Fromme P, Tourwé D, Schiller PW, Roth BL, Ballet S, Katritch V, Stevens RC, Cherezov V. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25686086">Structural basis for bifunctional peptide recognition at human δ-opioid receptor.</a>  <b>Nature Struct Mol Bio</b>. 22: 265-268, 2015</li>
</ol>
<b class="year">2014</b>
<ol>
	<li>Wang C, Wu H, Evron T, Vardy E, Han GW, Huang XP, Mangano TJ, Urban DJ, Katrich, V, Cherezov V, Caron MG, Roth BL and Stevens RC 2014. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25008467">Structural basis for Smoothened receptor modulation and chemoresistance to anticancer drugs.</a> <b>Nature Commun</b> 5:4355 2014</li>
	<li>Fenalti G, Giguere PM, Katritch V, Huang XP, Thompson AA, Cherezov V, Roth BL* and Stevens RC*. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/24413399">Molecular control of δ-opioid receptor signalling.</a> <b>Nature</b> 506(7487):191-6, 2014 (*Corresponding authors)</li>
</ol>
<b class="year">2013</b>
<ol>
	<li>Vardy E, Roth BL. 2013. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23374334">Conformational ensembles in GPCR activation.</a> <b>Cell</b> 152: 385-6</li>
	<li>Wacker D, Wang C, Katritch V, Han GW, Huang XP, Vardy E, McCorvy JD, Jiang Y, Chu M, Siu FY, Liu W, Xu HE, Cherezov V, Roth BL*, Stevens RC*.  <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23519215">Structural features for functional selectivity at serotonin receptors.</a> <b>Science</b> 340: 615-9, 2013 (*Corresponding authors)</li>
	<li>Lin H, Sassano MF, Roth BL*, Shoichet BK*. 2013. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23291723">A pharmacological organization of G protein-coupled receptors.</a> <b>Nature</b> Methods 10: 140-6, 2013 (*Corresponding authors)</li>
	<li>Wang C, Jiang Y, Ma J, Wu H, Wacker D, Katritch V, Han GW, Liu W, Huang XP, Vardy E, McCorvy JD, Gao X, Zhou XE, Melcher K, Zhang C, Bai F, Yang H, Yang L, Jiang H, Roth BL, Cherezov V, Stevens RC, Xu HE. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23519210">Structural basis for molecular recognition at serotonin receptors.</a> <b>Science</b> 340: 610-4, 2013</li>
	<li>Wang C, Wu H, Katritch V, Han GW, Huang XP, Liu W, Siu FY, <strong>Roth</strong> BL, Cherezov V, <strong>Stevens</strong> RC. <a target="_blank" href="https://www.ncbi.nlm.nih.gov/pubmed/23636324">Structure of the human smoothened receptor bound to an antitumour agent.</a> <b>Nature</b> 497(7449):338-43. doi: 10.1038/nature12167. Epub 2013 May 1. 2013 May 16.</li>
</ol>
<b class="year">2012</b>
<ol>
	<li>Roth, B.L., and Marshall, F.H. 
		<a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23222609">
		NOBEL 2012 Chemistry: Studies of a ubiquitous receptor family.</a>
		<b>Nature.</b> 492, 57, 2012
	</li>
	<li>Wu H, Wacker D, Mileni M, Katritch V, Han GW, Vardy E, Liu W, Thompson AA, Huang XP, Carroll FI, Mascarella SW, Westkaemper RB, Mosier PD, Roth BL, Cherezov V and Stevens RC (2012) <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/22437504">Structure of the human κ-opioid receptor in complex with JDTic.</a> <b>Nature</b> 485: 327-332, 2012.</li>
	<li>Besnard J, Ruda GF, Setola V, Abecassis K, Rodriguiz RM, Huang XP, Norval S, Sassano MF, Shin AI, Webster LA, Simeons FR, Stojanovski L, Prat A, Seidah NG, Constam DB, Bickerton GR, Read KD, Wetsel WC, Gilbert IH, Roth BL*, Hopkins AL*. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/23235874">Automated design of ligands to polypharmacological profiles.</a> <b>Nature</b> 492: 215-220, 2012 (*Corresponding authors)</li>
	<li>Thompson AA, Liu W, Chun E, Katritch V, Wu H, Vardy E, Huang XP, Trapella C, Guerrini R, Calo G, <strong>Roth BL</strong>, Cherezov V, <strong>Stevens</strong> RC.  <a target="_blank" href="http://www.ncbi.nlm.nih.gov/pubmed/22596163">Structure of the nociceptin/orphanin FQ receptor in complex with a peptide mimetic.</a> <b>Nature</b> 485(7398):395-9, 2012 May 16</li>
</ol>
<h1>Older Foundation Papers</h1>
<ol>
	<li>Huang, H-S, Allen JA, Mabb AM, King IF, Miriyala J, Taylor B, Sciaky N, Dutton JW, Lee H-M, Chen X, Jin J, Bridges AS, Zylka MJ*, Roth BL* and Philpot BD*.  <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/22190039">Topoisomerase inhibitors unsilence the dormant allele of Ube3a in neurons.</a> <b>Nature</b> 481: 185-189, 2011 (*Corresponding authors)
	</li>
	<li>Keiser MJ, Setola V, Irwin JJ, Laggner C, Abbas AI, Hufeisen SJ, Jensen NH, Kuijer MB, Matos RC, Tran TB, Whaley R, Glennon RA, Hert J, Thomas KL, Edwards DD, Shoichet BK*, Roth BL*. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/19881490">Predicting new molecular targets for known drugs.</a> <b>Nature</b>. Nov 12;462(7270):175-81, 2009 (*Corresponding authors)</li>
	<li>Alexander GM, Rogan SC, Abbas AI, Armbruster BN, Pei Y, Allen JA, Nonneman RJ, Hartmann J, Moy SS, Nicolelis MA, McNamara JO, Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/19607790">Remote control of neuronal activity in transgenic mice expressing evolved G protein-coupled receptors.</a> <b>Neuron</b>. Jul 16;63(1):27-39, 2009.</li>
	<li>BN Armbruster, X Li, S Herlitzer, M Pausch and BL Roth. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/17360345">Evolving the lock to fit the key to create a family of G protein-coupled receptors potently activated by an inert ligand.</a>. <b>Proc Natl Acad Sci</b> 104(12):5163-8, 2007.</li>
	<li>Keiser MJ, Roth BL, Armbruster BN, Ernsberger P, Irwin JJ, Shoichet BK. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/17287757">Relating protein pharmacology by ligand chemistry.</a> <b>Nature</b>  Biotechnol. 2007 Feb;25(2):197-206.</li>
	<li>BL Roth, K Baner, RB Westkaemper, KC Rice, S Steinberg, P Ernsberger, and RB Rothman. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/12192085">Salvinorin A: a potent naturally occurring nonnitrogenous kappa opioid selective agonist.</a> <b>Proc Natl Acad Sci USA</b>, 99: 11934-11939, 2002.</li>
	<li>RB Rothman, MH Baumann, JE Savage, L Rauser, A McBride, S Hufisein and BL Roth. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/11104741">Evidence for possible involvement of 5-HT(2B) receptors in the cardiac valvulopathy associated with fenfluramine and other serotonergic medications.</a><b>Circulation</b>. 102(23):2836-41, 2000.</li>
</ol>
<h1>Recent Reviews</h1>
<ol>
	<li>Urban DJ and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25292433">DREADDs (designer receptors exclusively activated by designer drugs): chemogenetic tools with therapeutic utility.</a> <b>Ann Rev Pharmacol Toxicol</b> 55: 399-417, 2015.</li>
	<li>Sternson SM and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/25002280">Chemogenetic tools to interrogate brain functions.</a> <b>Ann Rev Neurosi</b> 37: 387-407, 2014</li>
	<li>Giguere PM, Kroeze WK and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/24680430">Tuning up the right signal: chemical and genetic approaches to study GPCR functions.</a> <b>Curr Opin Cell Biol</b> 27: 51-55, 2014.</li>
	<li>Zhu H and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/24853931">Silencing synapses with DREADDs.</a> <b>Neuron</b> 82: 723-725, 2014.</li>
	<li>Allen JA and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/20868273">Strategies to discover unexpected targets for drugs active at G protein-coupled receptors.</a> <b>Ann Rev Pharmacol Toxicol</b> 51: 117-144, 2011.</li>
	<li>Berger M, Gray JA and Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/19630576">The expanded biology of serotonin.</a>  <b>Ann Rev Med</b> 60: 355-366, 2009.</li>
	<li>Roth BL. <a target="_blank" href="//www.ncbi.nlm.nih.gov/pubmed/17202450">Drugs and valvular heart disease.</a> <b>New England Journal</b> of Medicine 356: 6-9, 2007.</li>
</ol>
<?php require 'footer.php'?>
