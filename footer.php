</div>
<footer class="page-footer light-blue darken-2">
	<div class="container">
		<div class="row">
			<div class="col l6 s12">
                <h5 class="white-text">Rothlab</h5>
                <p class="grey-text text-lighten-4">Roth Lab is part of the Department of Pharmacology, a research department in the School of Medicine at the University of North Carolina at Chapel Hill.
				</p>
				<a class="white-text" href="mailto:estelalopez@unc.edu">estelalopez@unc.edu</a>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
            &copy; 2016 Copyright Roth Lab
		</div>
	</div>
</footer>
</body>
</html>	