<?php require 'header.php'?>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(4).addClass("active");</script>
<title>Roth Lab - Positions</title>

<h1>Available Lab Positions</h1>

<div class='row'>
	<div class='col s2'><h4>Position</h4></div>
	<div class='col s9'><h4>Description</h4></div>
	<div class='col s1'><h4>Contact</h4></div>
</div>
<div class='row'>
	<div class='col s2'><strong>Several Post-Doctoral</strong></div>
	<div class='col s9'>Dr. Roth's laboratory has interests in G-Protein Coupled Receptors, Signal Transduction and Drug Discovery. Highly experienced as well as newly graduated PhD's are encouraged to apply. Expertise in the pharmacology, cell signaling and/or biochemistry of GPCR's is desired. Knowledge of molecular biology and biochemistry, commensurate with experience, is required. <b>For structural biology positions, experience with protein purification and crystallography or cryo-EM is expected.</b><br><br>

<a target="_blank" href="https://pdspdb.unc.edu/rothlab/publications.php">Recent publications</a>. 

		</div>
	<div class='s1'><a href="mailto:bryan_roth@med.unc.edu">Bryan Roth</a></div>
</div>
<div class='row'>
	<div class='col s2'><strong>Graduate Student</strong></div>
	<div class='col offset-s9 s1'><a href="mailto:bryan_roth@med.unc.edu">Bryan Roth</a></div>
</div>
<div class='row'>
	<div class='col s2'><strong>PDSP Research Technician</strong></div>
	<div class='col s9'>The laboratory of Dr. Bryan Roth in UNC Dept of Pharmacology is seeking a graduate in a scientific field (biology, chemistry, biochemistry, etc.) as a PDSP Research Technician. The PDSP Receptor Binding Technician performs routine assays on a daily basis. Three major work responsibilities: <ol>
        <li>Perform experiments (binding assays, functional assays, etc.) according to established protocols. Ensure availability of all reagents and supplies for future experiments.</li>
        <li>Maintain data and experimental notes in an organized fashion with complete integrity. Under direction from supervisors utilize the PDPS Database to record and submit assay results and ensure proper assay performance.</li>
        <li>General lab tasks: communicate need for chemicals, supplies and lab materials, perform routine laboratory maintenance and cleaning.</li>
	</ol>
	<strong>Essential Skills, Knowledge and Abilities:</strong><br>
	Basic knowledge to perform duties and use scientific instruments. Conduct routine tests consistently with accuracy and precision. Maintain highly organized data and experimental records. Ability to manage multiple tasks simultaneously. Experience with pipetting required. Familiarity with PCâ€™s, MS office programs, e-mail clients and browsers. 
	<br><br>
	<strong>Preferred Qualifications: </strong><br>
	Bachelor's degree in a scientific field (biology, chemistry, biochemistry, etc.) from an accredited university. One year of laboratory experience in a biological field is preferred. A fine attention to detail is required for this work. Ability to follow published protocols. Fine acuity to measure small quantities of rare, expensive or fine chemicals. Long periods of standing may be involved. Required to work with radioactive materials. Must be able communicate effectively with supervisors 
	<br><br>
	This is a temporary, full time position that could become permanent.<br>
	Send resumes: <a href="mailto:estelaLopez@unc.edu">Estela Lopez</a><br>
	ADA/EOE employer.<br>
	</div>
	<div class='col s1'><a href="mailto:bryan_roth@med.unc.edu">Bryan Roth</a><br><br><a href="mailto:estelaLopez@unc.edu">Estela Lopez</a></div>
</div>



<?php require 'footer.php'?>
