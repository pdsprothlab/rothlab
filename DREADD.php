<?php require 'header.php'?>
<title>Roth Lab - DREADD</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(5).addClass("active");</script>

<h1>DREADDs links</h1>
<div class="row">
	<div class="col s6">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/dreadblog.png" class="materialboxed">
				<span class="card-title pink">Blog</span>
			</div>
            <div class="card-content">
				<p>DREADD users blog</p>
			</div>
            <div class="card-action">
				<a href="http://chemogenetic.blogspot.com/">Click HERE to Go There</a>
			</div>
		</div>
	</div>
	<div class="col s6">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/dreadwiki.png" class="materialboxed">
				<span class="card-title pink">Wiki</span>
			</div>
            <div class="card-content">
				<p>Welcome to the Designer Receptors Exclusively Activated by Designer Drugs DREADD wiki</p>
			</div>
            <div class="card-action">
				<a href="http://pdspit3.mml.unc.edu/projects/dreadd/wiki/WikiStart">Click HERE to Go There</a>
			</div>
		</div>
	</div>
</div>
<?php require 'footer.php'?>
