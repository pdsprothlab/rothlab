<?php require 'header.php'?>
<div class="row">
	<div class="col s6">
		<div class="card">
            <div class="card-content ">
				<span class="card-title">Info</span>
				<p>
					Roth Lab studies the structure and function of G-Protein coupled receptors (GPCRs).
					Roth Lab is part of the <a href="http://www.med.unc.edu/pharm/">Department of Pharmacology</a>,
					a research department in the <a href="http://www.med.unc.edu/">School of Medicine</a>
					at the <a href="http://www.unc.edu/">University of North Carolina at Chapel Hill</a>.
				</p>
			</div>
            <div class="card-action">
				<a href="research.php">Our Research</a>
				<a href="press.php">PRESS</a>
				<a href="members.php">Members</a>
				<a href="publications.php">Publications</a>
				<a href="positions.php">Open Positions</a>
			</div>
		</div>
		<div class="card">
            <div class="card-content">
				<span class="card-title">Resources</span>
				<p>
					For information about DREADDs visit our new DREADDs Page.
				</p>
			</div>
            <div class="card-action">
				<a href="../prestotango">PRESTO-Tango</a>
				<a href="http://chemogenetic.blogspot.com/">DREADDs Blog</a>
				<a href="http://pdspit3.mml.unc.edu/projects/dreadd/wiki/WikiStart">DREADDs Wiki</a>
				</div>
			</div>
		<div class="card">
		<div class="card-content">
		<span class="card-title">NIMH PDSP</span>
		<p>
		The Roth lab is the principal contractor for the NIMH Psychoactive Drug Screening Program which includes the PDSP Ki database.
		</p>
		</div>
		<div class="card-action">
		<a href="https://kidbdev.med.unc.edu/databases/pdsp.php">Ki Database</a>
		<a href="https://kidbdev.med.unc.edu/databases/snidd/">Tracer Database</a>
		<a href="https://kidbdev.med.unc.edu/databases/ShaunCell/home.php">Anat. Profiling GPCR</a>
		<a href="../pdspweb">NIMH PDSP</a>
		<a href="UNClinks.php">UNC</a>
		</div>
		</div>
		</div>
		<div class="col s6">
		<div class="card">
		<div class="card-content">
		<span class="card-title">Recent News:</span>
		<?php
		$xml = simplexml_load_file("press.xml");
		for($i=0;$i<5;$i++){
		$tag = $xml->article[$i];
		if($tag->image == "") $tag->image = "pictures/noavatar.png";
		echo '<div class="row"><div class="col s2"><a target="_blank" href="'.$tag->url.'"><img class="responsive-img" alt="urlimage" src="'.$tag->image.'"></a></div><div class="col s10"><a target="_blank" href="'.$tag->url.'">'.$tag->name.'</a><br><em>'.$tag->date.'</em> <br><span>'.$tag->press.'</span></div></div><br>';
		}
		?>
		</div>
		</div>
		</div>
		</div>
		
		<?php require 'footer.php'?>		
