<?php require 'header.php'?>
<title>Roth Lab - Databases</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(6).addClass("active");</script>

<h1>Databases</h1>

<div class="row">
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/kidb.png" class="materialboxed">
				<span class="card-title pink">Ki DB</span>
			</div>
            <div class="card-content">
				<p>Ki Database</p>
			</div>
            <div class="card-action">
				<a href="https://pdsp.unc.edu/databases/kidb.php">Click HERE to Go There</a>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/tracerdb.png" class="materialboxed">
				<span class="card-title pink">Tracer DB</span>
			</div>
            <div class="card-content">
				<p>Tracer Database</p>
			</div>
            <div class="card-action">
				<a href="https://pdsp.unc.edu/databases/snidd/">Click HERE to Go There</a>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/gcprdb.png" class="materialboxed">
				<span class="card-title pink">GPCR DB</span>
			</div>
            <div class="card-content">
				<p>GCPR Database</p>
			</div>
            <div class="card-action">
				<a href="https://pdsp.unc.edu/databases/ShaunCell/home.php">Click HERE to Go There</a>
			</div>
		</div>
	</div>
</div>

<?php require 'footer.php'?>
