<?php require 'header.php'?>
<title>Roth Lab - UNC Links</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(8).addClass("active");</script>

<h1>UNC links</h1>

<div class="row">
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/uncmain.png" class="materialboxed">
				<span class="card-title pink">UNC</span>
			</div>
            <div class="card-content">
				<p>Home page of UNC</p>
			</div>
            <div class="card-action">
				<a href="http://unc.edu/">Click HERE to Go There</a>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/uncmed.png" class="materialboxed">
				<span class="card-title pink">UNC Medicine</span>
			</div>
            <div class="card-content">
				<p>UNC school of medicine</p>
			</div>
            <div class="card-action">
				<a href="http://med.unc.edu/">Click HERE to Go There</a>
			</div>
		</div>
	</div>
	<div class="col s4">
		<div class="card hoverable">
			<div class="card-image">
				<img src="pictures/uncpharm.png" class="materialboxed">
				<span class="card-title pink">PHARM</span>
			</div>
            <div class="card-content">
				<p>UNC Pharmacology</p>
			</div>
            <div class="card-action">
				<a href="http://www.med.unc.edu/pharm">Click HERE to Go There</a>
			</div>
		</div>
	</div>
</div>

<?php require 'footer.php'?>
