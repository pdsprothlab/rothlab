<?php require 'header.php'?>
<title>Roth Lab - Members</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(2).addClass("active");
	var sav1=[], sav2=[], sav6=[], sav4=[], sav5=[];

	
		$(document).ready(function(){
			$('a[href^="#"]').on('click',function (e) {
				e.preventDefault();
				
				var target = this.hash;
				var $target = $(target);
				
				$('html, body').stop().animate({
					'scrollTop': $target.offset().top
					}, 900, 'swing', function () {
					window.location.hash = target;
				});
			});
		}); // close out script
	</script>
	<h1>Members List</h1>
	<div class="row">
        <div class="input-field col s12">
			<input type="text" class="validate" id="filter">
			<label for="filter">Filter Members</label>
		</div>
	</div>
	<h5><span class='yellow'>*(color)</span> Denotes a member of the PDSP - <a href='../pdspweb/'>Psychoactive Drug Screening Program</h5>
		<a id='menae' class="waves-effect waves-light btn blue" href='#CurrentMembers'>Current Members</a>
		<a id='menad' class="waves-effect waves-light btn orange" href='#FormerStudents'>Former Students</a>
		<!-- <a id='menac' class="waves-effect waves-light btn green" href='#Collaborators'>Collaborators</a> -->
		<a id='menab' class="waves-effect waves-light btn pink" href='#FormerPostDocs'>Former Post-Docs</a>
		<a id='menaa' class="waves-effect waves-light btn red" href='#VisitingScientists'>Visiting Scientists</a>
		<div id="memb">
			<h2 id='CurrentMembers'>Current Members</h2>
			<?php
				$xml = simplexml_load_file("members.xml");
				$k = 0;
				$members = array(array());
				foreach ($xml->user as $user) {
					$members[$k]["personID"] = trim(preg_replace('/\s\s+/','',$user -> column_1));
					$members[$k]["classificationID"] = trim(preg_replace('/\s\s+/','',$user -> column_2));
					$members[$k]["firstName"] = trim(preg_replace('/\s\s+/','',$user -> column_3));
					$members[$k]["lastName"] = trim(preg_replace('/\s\s+/','',$user -> column_4));
					$members[$k]["title"] = trim(preg_replace('/\s\s+/','',$user -> column_5));
					$members[$k]["email"] = trim(preg_replace('/\s\s+/','',$user -> column_6));
					$members[$k]["hometown"] = trim(preg_replace('/\s\s+/','',$user -> column_7));
					$members[$k]["research"] = trim(preg_replace('/\s\s+/','',$user -> column_8));
					$members[$k]["futureResearch"] = trim(preg_replace('/\s\s+/','',$user -> column_9));
					$members[$k]["otherInterests"] = trim(preg_replace('/\s\s+/','',$user -> column_10));
					$members[$k]["isPDSP"] = trim(preg_replace('/\s\s+/','',$user ->column_11));
					$members[$k]["isCurrent"] = trim(preg_replace('/\s\s+/','',$user ->column_13));
					$members[$k]["pictureID"] = trim(preg_replace('/\s\s+/','',$user ->column_14));
					$k++;
				}
				$members = array_filter($members, function(array $a)
				{
					return $a["classificationID"] != 3;
				});
				$members = array_filter($members, function(array $a)
				{
					if($a["classificationID"] == 1){
						return $a["isCurrent"] != 'N';
					}return true;
				});
				usort($members, function(array $a, array $b)
				{
					if($a["classificationID"] - $b["classificationID"] == 0){
						return strcmp($a["firstName"], $b["firstName"]);
					}
					return $a["classificationID"] - $b["classificationID"];
				});
			?>
			<?php
				$kc_level = 1;
				$string32 = "<div class='row'><ul class=\"collection\">";
				$countme = 0;
				foreach ($members as $user) {
					$countme++;
					if($kc_level < $user['classificationID']){
						if($kc_level == 2) $kc_level++;
						$kc_level++;
						if($kc_level == 2){
							$string32 .= "</ul></div><br/><h2 id='FormerStudents'>Former Students</h2><div class='row'><ul class=\"collection\">";
							}
							//else if($kc_level == 4){
							//$string32 .= "</ul></div><br/><h2 id='Collaborators'>Collaborators</h2><div class='row'><ul class=\"collection\">";
							//}
							else if($kc_level == 5){
							$string32 .= "</ul></div><br/><h2 id='FormerPostDocs'>Former Post-Docs</h2><div class='row'><ul class=\"collection\">";
							}else if($kc_level == 6){
							$string32 .= "</ul></div><br/><h2 id='VisitingScientists'>Visiting Scientists</h2><div class='row'><ul class=\"collection\">";
						}
					}
					if($user['isPDSP'] == "Y") $ispdsp = 'yellow';
					else $ispdsp = '';
					$string33 = "";
					
					$string33 .= "<li class=\"collection-item avatar\"><div class=\"row valign-wrapper\">";
					if(($user["pictureID"] != "N") && ($user["pictureID"] != "")){
						$string33 .= ' <div class="col s2"><img src="pictures/avatars/'.$user["pictureID"].'" alt="avatar" class="responsive-img materialboxed"></div>';
					}
					else
					{
						$string33 .= ' <div class="col s2"><img src="pictures/noavatar.png" alt="noavatar" class="responsive-img materialboxed"></div>';
					}
					$string33 .= ' <div class="col s10"><span class="title '.$ispdsp.'">'.$user['firstName'].' '.$user['lastName'].'</span><p>';
					if($user['title'] != "") $string33 .= "Title: ".$user['title']."<br/>";
					if($user['email'] != "") $string33 .= "Email: ".$user['email']."<br/>";
					if($user['hometown'] != "") $string33 .= "Hometown: ".$user['hometown']."<br/>";
					if($user['research'] != "") $string33 .= "Research: ".$user['research']."<br/>";
					if($user['futureResearch'] != "") $string33 .= "Future Research: ".$user['futureResearch']."<br/>";
					if($user['otherInterests'] != "") $string33 .= "Other Interests: ".$user['otherInterests']."<br/>";
					$string33 .= '</p></div></div></li>';					
					$string33 .= "<script>sav{$kc_level}.push('".addslashes($string33)."')</script>";
					$string32 .= $string33;
					unset($ispdsp);
				}
				$string32 .= "</ul></div>";
				echo $string32;
				echo "<script>document.getElementById('menae').innerHTML = 'Current Members ('+sav1.length+')'</script>";
				echo "<script>document.getElementById('menad').innerHTML = 'Former Students ('+sav2.length+')'</script>";
				//echo "<script>document.getElementById('menac').innerHTML = 'Collaborators ('+sav4.length+')'</script>";
				echo "<script>document.getElementById('menab').innerHTML = 'Former Post-Docs ('+sav5.length+')'</script>";
				echo "<script>document.getElementById('menaa').innerHTML = 'Visiting Scientists ('+sav6.length+')'</script>";
		?>
		<script>
			$(function(){
				$("#filter").keyup(function(){
					var str1="", str2="", str4="", str5="", str6="";
					var trval = $(this).val();
					var counter = 0;
					sav1.forEach(function(element,index,array){
						if($(element).find('.title').text().search(trval) != -1){ 
							++counter;
							str1 += element;
						}
					}); 
					document.getElementById('menae').innerHTML = 'Current Members ('+counter+')';
					if(str1 != ""){ str1 = "<h2 id='Current Members'>Current Members</h2><div class='row' ><ul class=\"collection\">" + str1 + "</ul></div>";}
					counter = 0;
					sav2.forEach(function(element,index,array){
						if($(element).find('.title').text().search(trval) != -1){ 
							++counter;
							str2 += element;
						}
					}); 
					document.getElementById('menad').innerHTML = 'Former Students ('+counter+')';
					if(str2 != ""){ str2 = "<br/><h2 id='Alumni'>Alumni</h2><div class='row'><ul class=\"collection\">" + str2 + "</ul></div>";}
					
					counter = 0;
					sav4.forEach(function(element,index,array){
						if($(element).find('.title').text().search(trval) != -1){ 
							++counter;
							str4 += element;
						}
					}); 
					/*
					document.getElementById('menac').innerHTML = 'Collaborators ('+counter+')';
					if(str4 != ""){ str4 = "<br/><h2 id='Collaborators'>Collaborators</h2><div class='row' ><ul class=\"collection\">" + str4 + "</ul></div>";}

					counter = 0;
					sav5.forEach(function(element,index,array){
						if($(element).find('.title').text().search(trval) != -1){ 
							++counter;
							str5 += element;
						}
					});
					*/ 
					document.getElementById('menab').innerHTML = 'Former Post-Docs ('+counter+')';
					if(str5 != ""){ str5 = "<br/><h2 id='PostDocs'>Post-Docs</h2><div class='row' ><ul class=\"collection\">" + str5 + "</ul></div>";}
					
					counter = 0;
					sav6.forEach(function(element,index,array){
						if($(element).find('.title').text().search(trval) != -1){ 
							++counter;
							str6 += element;
						}
					}); 
					document.getElementById('menaa').innerHTML = 'Visiting Scientists ('+counter+')';
					if(str6 != ""){ str6 = "<br/><h2 id='VisitingScientists'>Visiting Scientists</h2><div class='row' ><ul class=\"collection\">" + str6 + "</ul></div>";}
					$("#memb").html(str1 + str2 + str4 + str5 + str6);
					$('.materialboxed').materialbox();
				});
			});
		</script>
	</div>
	<?php require 'footer.php'?>
