<?php require 'header.php'?>
<title>Roth Lab - Research</title>
<script>$(".nav-wrapper").eq(0).children("ul").eq(0).children("li").eq(0).addClass("active");</script>

<h1>Roth Lab Research</h1>




<h3>Designing and engineering proteins</h3>
<div class="row">
	<div class="col s6">We have longstanding interests in the design, engineering, and evolution of proteins for designated functions.
	</div>
</div>

<h5><b>Chemogenetics:</b></h5> 
<img class="responsive-img materialboxed" src="pictures/chemicalGen2.png" align="left" style="max-width:300px;height:auto;"/>
<p>
	The Roth lab perfected the chemogenetic technology we have named “DREADD” (Designer Receptor Exclusively Activated by Designer Drugs; <a href="https://www.pnas.org/content/104/12/5163">Armbruster et al, 2007</a>). DREADD technology has afforded 1000’s of labs world-wide the opportunity to discover how cell-type specific modulation of signaling is translated into behavioral and non-behavioral outcomes (see <a href="https://www.sciencedirect.com/science/article/pii/S0896627316000659">Roth Neuron 2015</a> for recent review)
</p>

<p>The Roth lab continues to enhance DREADD technology (see <a href="https://pubmed.ncbi.nlm.nih.gov/25937170/">Vardy et al, Neuron 2015</a>; <a href="https://www.biorxiv.org/content/10.1101/854513v2">Ngai et al, Nature Neurosci <i>in press</i></a>).</p>

<div class="row">
	<div class="col s6">We also have made DREADD technology available to the scientific community:
		<ul>
			<li>All DREADD-related plasmids are available via <a target="_blank" href="https://www.addgene.org/Bryan_Roth/">ADDGENE</a></li>
			

			<li>High titer viral stocks available from <a target="_blank" href="https://www.addgene.org/viral-service/aav-prep/">ADDGENE</a></li>

		</ul>
	</div>
</div>


<h5><b>Directed Evolution:</b></h5>
<div class="row">
	<img class="responsive-img materialboxed" src="pictures/directedEvolution.jpeg" align="left"/>
	<p>We have created new technologies for directed molecular evolution in mammalian cells: VEGAS (Viral Evolution of Genetically Actuated Sequences; <a href="https://www.cell.com/cell/pdf/S0092-8674(19)30622-1.pdf">English et al, Cell 2019</a>). This technology provides a platform for the evolution of mammalian proteins towards defined objectives and has been exemplified with transcription factors, nanobodies, and signaling molecules.
	</p>
</div>

<h5><b>Nanobody Sensors:</b></h5>
<div class="row">
	<img class="responsive-img materialboxed" src="pictures/nanobody.png" align="left"/>
	<p>We have created nanobody based sensors to monitor the activation states of G protein coupled receptors (<a href="https://pubmed.ncbi.nlm.nih.gov/29307491/">Che et al, Cell 2018</a>; <a href="https://www.nature.com/articles/s41467-020-14889-7">Che et al, Nature Comm 2020</a>).
	</p>
</div>

<h5><b>G Protein Sensors:</b></h5>
<div class="row">
	<img class="responsive-img materialboxed" src="pictures/gprotein.png" align="left"/>
	<p>We have created a suite of BRET-based sensors to interrogate the transducerome for GPCRs (<a href="https://pmlegacy.ncbi.nlm.nih.gov/pubmed/32367019">Olsen et al, Nature Chem Bio 2020</a>)
	</p>
</div>

<br>
<h3>Structural-guided Discovery of Novel Ligands for G Protein Coupled Receptors</h3>
<br>

<h5><b>Structural Elucidation of GPCR Structures:</b></h5> 
<div class="row">
	<img class="responsive-img materialboxed" src="pictures/gpcrstructures.png" align="left" style="max-width:50%;height:auto;"/>
	<p>Recently solved GPCR structures include the structure of LSD bound to a serotonin receptor (<a href="https://www.cell.com/fulltext/S0092-8674(16)31749-4">Wacker et al, Cell 2018</a>), the active (<a href="https://pubmed.ncbi.nlm.nih.gov/29307491/">Che et al, Cell 2018</a>), and inactive (<a href="https://www.nature.com/articles/s41467-020-14889-7">Che et al, Nature Comm 2020</a>) states of the kappa opioid receptors and many others (Figure below). 
   <i><b>We also have recently solved several structures of GPCRs in complex with transducers by cryo-EM.</b></i>
	</p>
</div>

<h5><b>Structure-guided Discovery of New Chemical Tools for GPCRs:</b></h5>
<div class="row">
	<img class="responsive-img materialboxed" src="pictures/chemicaltools.jpeg" align="left"/>
	<p>Using these high-resolution structures we have discovered novel chemotypes for a number of GPCRs including the D4-dopamine receptor (<a href="https://pubmed.ncbi.nlm.nih.gov/29051383/">Wang et al, Science 2017</a>; <a href="https://pubmed.ncbi.nlm.nih.gov/30728502/">Lyu et al, Nature 2019</a>), MT1 and MT2 melatonin receptors (<a href="https://www.nature.com/articles/s41586-020-2027-0">Stein, Kang et al, Nature 2020</a>) and D2 dopamine receptors (<a href="https://www.nature.com/articles/nchembio.2527">McCory et al Nature Chem Bio 2017</a>).
	</p>
</div>

<p>
	Ongoing projects are aimed at utilizing these structures to discover new therapies for a number of neuropsychiatric disorders.
</p>

<?php require 'footer.php'?>
